import typing
from collections.abc import Iterable

from sqlalchemy import create_engine, select
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm import sessionmaker

from src.db import Base


class DBClient(object):
    def __init__(self, user: str, password: str, db: str, host: str, port: int = 5432) -> None:
        engine = create_engine(f'postgresql://{user}:{password}@{host}:{port}/{db}', echo=True)
        Base.metadata.create_all(engine)
        self._session = sessionmaker(bind=engine)

    def __enter__(self):
        self.session = self._session()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            print(exc_type)
        if exc_val:
            print(exc_val)
        if exc_tb:
            print(exc_tb)
        self.session.close()

    @classmethod
    def upsert(cls, session: typing.Any, model: typing.Any, row: typing.Any) -> None:
        statement = insert(model).values(**row.as_dict()).on_conflict_do_update(
            index_elements=[row.get_primary_key_name()],
            set_=row.as_dict(without_prime=True))
        session.execute(statement)
        session.commit()

    @classmethod
    def select(cls, session: typing.Any,
               model: typing.Tuple,
               join: typing.Tuple = None,
               where: typing.Optional[typing.Any] = None,
               filter_cond: typing.Optional[typing.Union[typing.Iterable, typing.Any]] = None,
               return_statement_only: bool = False) -> typing.Any:
        statement = session.query(*model)
        # statement = select(*model)
        if join is not None:
            statement = statement.join(*join)
        if where is not None:
            statement = statement.where(where)
        if filter_cond is not None:
            if not isinstance(filter_cond, Iterable):
                filter_cond = [filter_cond]
            for _f in filter_cond:
                statement = statement.filter(_f)
        if return_statement_only:
            return statement
        return statement
