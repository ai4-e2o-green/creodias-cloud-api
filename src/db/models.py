import typing

from pydantic import BaseModel
from geoalchemy2 import Geometry
from sqlalchemy import Column, String, DateTime, Numeric, Integer, ForeignKey
from sqlalchemy.inspection import inspect

from src.db import Base


class Polygon(BaseModel):
    name: str
    coordinates: typing.List[typing.List[typing.List]]


class ModelUtils(object):
    def as_dict(self, without_prime: bool = False) -> typing.Mapping:
        exclude = ['_sa_instance_state']
        if without_prime:
            exclude.append(self.get_primary_key_name())
        _d = vars(self)
        return {_key: _d[_key] for _key in _d if _key not in exclude}

    @classmethod
    def get_primary_key_name(cls):
        return inspect(cls).primary_key[0].name


class EODataPolygons(ModelUtils, Base):
    __tablename__ = 'polygons'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    coordinates = Column(Geometry)


class EODataIndexV2(ModelUtils, Base):
    __tablename__ = 'eodata_index_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifier = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    title = Column(String)
    organisationName = Column(String)
    collection = Column(String)
    startDate = Column(DateTime)
    completionDate = Column(DateTime)
    productType = Column(String)
    processingLevel = Column(String)
    platform  = Column(String)
    instrument = Column(String)
    updated = Column(DateTime)
    published = Column(DateTime)
    snowCover = Column(Numeric)
    cloudCover = Column(Numeric)


class EODataCroppedV2(ModelUtils, Base):
    __tablename__ = 'eodata_cropped_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifierCropped = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    productIdentifier = Column(String)


class EODataPanSharpeningV2(ModelUtils, Base):
    __tablename__ = 'eodata_pansharpening_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifierPansharpening = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    productIdentifierType = Column(String)
    productIdentifier = Column(String)
    precision = Column(String)


class EODataNdviV2(ModelUtils, Base):
    __tablename__ = 'eodata_ndvi_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifierNdvi = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    productIdentifier = Column(String)
    minValue = Column(Numeric)
    maxValue = Column(Numeric)


class EODataNdmiV2(ModelUtils, Base):
    __tablename__ = 'eodata_ndmi_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifierNdmi = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    productIdentifier = Column(String)
    minValue = Column(Numeric)
    maxValue = Column(Numeric)


class EODataLaiV2(ModelUtils, Base):
    __tablename__ = 'eodata_lai_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifierLai = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    productIdentifier = Column(String)
    minValue = Column(Numeric)
    maxValue = Column(Numeric)


class EODataStackedV2(ModelUtils, Base):
    __tablename__ = 'eodata_stacked_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifier = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    productIdentifierStacked = Column(String)


class EODataCDV2(ModelUtils, Base):
    __tablename__ = 'eodata_cd_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifier = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    minValue = Column(Numeric)
    maxValue = Column(Numeric)
    productIdentifierReferent = Column(String)
    productIdentifierTarget = Column(String)
    productIdentifierCD = Column(String)


class EODataEvapV2(ModelUtils, Base):
    __tablename__ = 'eodata_evap_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifierEvap = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    productIdentifier = Column(String)
    minValue = Column(Numeric)
    maxValue = Column(Numeric)
    seasonType = Column(String)


class EODataCogV2(ModelUtils, Base):
    __tablename__ = 'eodata_cog_v2'
    __table_args__ = {'extend_existing': True}
    productIdentifierCog = Column(String, primary_key=True)
    polygonId = Column(Integer, ForeignKey(EODataPolygons.id))
    productIdentifier = Column(String)
    productIdentifierIndex = Column(String)
    productIdentifierIndexType = Column(String)
