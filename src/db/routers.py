from json import loads, dumps

from fastapi import APIRouter, status
from geoalchemy2.functions import ST_GeomFromGeoJSON, ST_AsGeoJSON, ST_GeomFromWKB

from src.db.client import DBClient
from src.settings import (
    DBSettings,
    INDEX_CHOICE,
    INDEX_MODELS
)
from src.db.models import EODataIndexV2, EODataCogV2, EODataPolygons, Polygon
from src.exceptions import NonExistingIndexException

router = APIRouter(
    prefix='/db',
    tags=['db']
)

db_settings = DBSettings()


@router.get("/index_props/{index_type}/", tags=["index_type"])
async def get_indexes(index_type: str):
    index_type = index_type.lower()
    if index_type not in INDEX_CHOICE:
        raise NonExistingIndexException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"index_type: {index_type} is not recognize, allowed types are: {INDEX_CHOICE}"
        )
    model = INDEX_MODELS[index_type]
    eodata_rows = []
    index_type = index_type if index_type != 'cd' else index_type.upper()
    with DBClient(**db_settings.dict()) as session:
        query_eodata_index = DBClient.select(
            session=session,
            model=(EODataCogV2, EODataIndexV2),
            join=(EODataIndexV2, (
                      (EODataCogV2.productIdentifier == EODataIndexV2.productIdentifier) &
                      (EODataCogV2.polygonId == EODataIndexV2.polygonId)
                  )),
            filter_cond=(EODataCogV2.productIdentifierIndexType == index_type)
        )
        for _row in query_eodata_index.all():
            model_data, eo_data = _row[0].as_dict(), _row[1].as_dict()
            with DBClient(**db_settings.dict()) as session:
                model_index_data = DBClient.select(
                    session=session,
                    model=(model,),
                    filter_cond=[model.productIdentifier == model_data['productIdentifier'],
                                 model.polygonId == model_data['polygonId']]
                ).all()
                for _item in model_index_data:
                    eodata_rows.append({
                        'productIdentifier': model_data['productIdentifier'],
                        'startDate': eo_data['startDate'],
                        'completionDate': eo_data['completionDate'],
                        'published': eo_data['published'],
                        'targetPath': model_data['productIdentifierCog'],
                        'polygonId': model_data['polygonId'],
                        'minValue': _item.minValue,
                        'maxValue': _item.maxValue
                    })
    return eodata_rows


@router.get("/index_props/{index_type}/{polygon_id}", tags=["index_type"])
async def get_indexes_by_polygon_id(index_type: str, polygon_id: int):
    index_type = index_type.lower()
    if index_type not in INDEX_CHOICE:
        raise NonExistingIndexException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"index_type: {index_type} is not recognize, allowed types are: {INDEX_CHOICE}"
        )
    model = INDEX_MODELS[index_type]
    eodata_rows = []
    index_type = index_type if index_type != 'cd' else index_type.upper()
    with DBClient(**db_settings.dict()) as session:
        query_eodata_index = DBClient.select(
            session=session,
            model=(EODataCogV2, EODataIndexV2),
            join=(EODataIndexV2, (
                      (EODataCogV2.productIdentifier == EODataIndexV2.productIdentifier) &
                      (EODataCogV2.polygonId == EODataIndexV2.polygonId)
                  )),
            filter_cond=[EODataCogV2.productIdentifierIndexType == index_type, EODataCogV2.polygonId == polygon_id]
        )
        for _row in query_eodata_index.all():
            model_data, eo_data = _row[0].as_dict(), _row[1].as_dict()
            with DBClient(**db_settings.dict()) as session:
                model_index_data = DBClient.select(
                    session=session,
                    model=(model,),
                    filter_cond=[model.productIdentifier == model_data['productIdentifier'],
                                 model.polygonId == model_data['polygonId']]
                ).all()
                for _item in model_index_data:
                    eodata_rows.append({
                        'productIdentifier': model_data['productIdentifier'],
                        'startDate': eo_data['startDate'],
                        'completionDate': eo_data['completionDate'],
                        'published': eo_data['published'],
                        'targetPath': model_data['productIdentifierCog'],
                        'polygonId': model_data['polygonId'],
                        'minValue': _item.minValue,
                        'maxValue': _item.maxValue
                    })
    return eodata_rows


@router.get("/polygons/all", tags=['polygon'])
async def get_polygons():
    results = {'polygones': []}
    with DBClient(**db_settings.dict()) as session:
        query_eodata_polygons_index = DBClient.select(
            session=session,
            model=(EODataPolygons,)
        )
        for _row in query_eodata_polygons_index.all():
            data = _row.as_dict()
            data['coordinates'] = loads(DBClient.select(
                session=session,
                model=(ST_AsGeoJSON(data['coordinates'].desc),)
            ).scalar())['coordinates']
            results['polygones'].append({
                'name': data['name'],
                'coordinates': data['coordinates'],
                'polygonId': data['id']
            })
    return results


@router.post("/polygons/add", tags=["polygon"])
async def add_polygon(polygon: Polygon):
    with DBClient(**db_settings.dict()) as session:
        row = polygon.dict()
        row['coordinates'] = ST_GeomFromGeoJSON(dumps({"type": "Polygon", "coordinates": row["coordinates"]}))
        DBClient.upsert(session=session, model=EODataPolygons, row=EODataPolygons(**row))
