from fastapi import FastAPI

import src.exceptions as exceptions
from src.db import routers as db_routers
from src.storage import routers as storage_routers
from src.middlewares import add_middlewares


app = FastAPI()
add_middlewares(app=app)
exceptions.include_app(app=app)
app.include_router(router=db_routers.router)
app.include_router(router=storage_routers.router)


@app.get(path='/')
def home():
    return {'msg': 'Welcome to the home page!'}
