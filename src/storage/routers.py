import pathlib

from fastapi import APIRouter, status
from starlette.responses import FileResponse


from src.exceptions import NonExistingFilePathException
from src.storage.models import FilePath


router = APIRouter(
    prefix='/storage',
    tags=['storage']
)


@router.post("/download/", tags=["download"])
async def get_indexes(file_path: FilePath):
    path = pathlib.Path(file_path.path)
    if not path.exists():
        raise NonExistingFilePathException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"file: '{path}' does not exist."
        )
    return FileResponse(str(path.absolute()), media_type="multipart/form-data", filename=str(path.name))


@router.get("/download/", tags=["download"])
async def get_indexes(file_path: str):
    path = pathlib.Path(file_path)
    if not path.exists():
        raise NonExistingFilePathException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"file: '{path}' does not exist."
        )
    return FileResponse(str(path.absolute()), media_type="multipart/form-data", filename=str(path.name))
