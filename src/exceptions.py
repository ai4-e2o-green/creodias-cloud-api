from fastapi import (
    HTTPException,
    Request
)
from starlette.responses import JSONResponse


class NonExistingIndexException(HTTPException):
    pass


def non_existing_index_exception_handler(request: Request, exc: NonExistingIndexException):
    return JSONResponse(status_code=exc.status_code, content={"message": exc.detail})


class NonExistingFilePathException(HTTPException):
    pass


def non_existing_file_path_exception_handler(request: Request, exc: NonExistingFilePathException):
    return JSONResponse(status_code=exc.status_code, content={"message": exc.detail})


def include_app(app):
    app.add_exception_handler(NonExistingIndexException, non_existing_index_exception_handler)
    app.add_exception_handler(NonExistingFilePathException, non_existing_file_path_exception_handler)
