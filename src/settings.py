from typing import Any

from pydantic import BaseSettings

from src.db.models import (
    EODataNdviV2,
    EODataNdmiV2,
    EODataLaiV2,
    EODataCDV2,
    EODataEvapV2
)


class DBSettings(BaseSettings):
    user: str = 'postgres'
    password: str = '0605DAPostgis'
    host: str = '64.225.133.59'
    db: str = 'gis'

    class Config:
        env_prefix = 'APP_'


INDEX_CHOICE = {'ndvi', 'ndmi', 'lai', 'cd', 'evap'}
INDEX_MODELS = {
    'ndvi': EODataNdviV2,
    'ndmi': EODataNdmiV2,
    'lai': EODataLaiV2,
    'cd': EODataCDV2,
    'evap': EODataEvapV2
}
